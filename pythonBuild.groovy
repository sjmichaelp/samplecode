// vars/pythonBuild.groovy
def call() {
	pipeline {    
	agent any     
	stages {
	    stage('Lint') {            
			steps {             
				sh 'pylint-fail-under --fail_under 5.0 *.py'
			}        
		}
        stage('Build') {            
			steps {                
				sh 'pip install -r requirements.txt'             
			}        
		}
		stage('Test') {            
			steps {           
				script {
				    def files = findFiles(glob: 'test*.py')
				    for (int i = 0; i < files.size(); i++) {
				        sh "coverage run --omit=*/site-packages/*,*/dist-packages/* ${files[i].name}"
				        sh "coverage report"
				    }
				}
			}        
			post {
				always {
				    script {
				        exists = fileExists 'test-reports'
				        if (exists) {
    					    junit 'api-test-reports/*.xml'
				        }
				        
				        exists = fileExists 'api-test-reports'
				        if (exists) {
    					    junit 'test-reports/*.xml'
				        }
				    }

				}
			}    
		}
		stage('Package') {
		    steps {
		        sh 'zip app.zip *.py'
		        archiveArtifacts artifacts: 'app.zip', fingerprint: true
		    }
		}
	} 
}
}